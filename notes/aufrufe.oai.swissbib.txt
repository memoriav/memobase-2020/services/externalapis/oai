
ListRecords
http://oai.swissbib.ch/oai/DB=2.1?verb=ListRecords&metadataPrefix=m21-xml%2Foai&set=A&from=2016-01-01&until=2016-02-61


ListSets
http://oai.swissbib.ch/oai/DB=2.1?verb=ListSets


ListIdentifiers
swissbib verlangt from was nicht korrekt ist
http://oai.swissbib.ch/oai/DB=2.1?verb=ListIdentifiers&metadataPrefix=m21-xml%2Foai&from=2016-01-01
http://oai.swissbib.ch/oai/DB=2.1?verb=ListIdentifiers&metadataPrefix=m21-xml%2Foai&from=2016-01-01&set=F

GetRecord
http://oai.swissbib.ch/oai/DB=2.1?verb=GetRecord&identifier=175005036&metadataPrefix=m21-xml%2Foai


ListMetadataFormats
http://oai.swissbib.ch/oai/DB=2.1?verb=ListMetadataFormats

Identify
http://oai.swissbib.ch/oai/DB=2.1?verb=Identify


gültige Aufrufe
http://localhost:9000/?verb=GetRecord&metadataPrefix=myprefix&identifier=1234
http://localhost:9000/?verb=ListSets
http://localhost:9000/?verb=ListSets&resumptionToken=1234
http://localhost:9000/?verb=Identify


Aufrufe ES scroll api
POST documents-v8/_search?scroll=1m
{
  "size": 100,
  "query": {
    "match_all": {}
  }

}


POST /_search/scroll
{
  "scroll" : "1m",
  "scroll_id" : "DnF1ZXJ5VGhlbkZldGNoAwAAAAAAAB5dFlp2eUpoaWZzUURTMGRteTl6bHN0ckEAAAAAAAYITRZuWVYyeWFBWlNDQzhsQ3RBd0dDZ1BBAAAAAAAAHl4WWnZ5SmhpZnNRRFMwZG15OXpsc3RyQQ=="
}

_scroll_id in response
{
  "_scroll_id" : "DnF1ZXJ5VGhlbkZldGNoAwAAAAAAAB5dFlp2eUpoaWZzUURTMGRteTl6bHN0ckEAAAAAAAYITRZuWVYyeWFBWlNDQzhsQ3RBd0dDZ1BBAAAAAAAAHl4WWnZ5SmhpZnNRRFMwZG15OXpsc3RyQQ==",
  "took" : 9,

