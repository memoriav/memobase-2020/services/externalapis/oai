
Hinweis: metadataPrefix Argumente sind willkürlich gewählt, bisher nicht festgelegt

GetRecord
https://oai.memobase.k8s.unibas.ch/?verb=GetRecord&identifier=https://memobase.ch/record/RadioX-Kampagnen-DFB2B95FCB3A21255FCF10428_mpg&metadataPrefix=1234


ListRecords
https://oai.memobase.k8s.unibas.ch/?verb=ListRecords&metadataPrefix=oai_rfc1807
mit anschliessendem resumption token (max. 2 Minuten Zeitdauer zwischen den Aufrufen, sonst wird das token ungültig)
https://oai.memobase.k8s.unibas.ch/?verb=ListRecords&resumptionToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzM4NCJ9.eyJpc3MiOiJtZW1vYmFzZSBPQUkgc2VydmVyIiwic3ViIjoiRG5GMVpYSjVWR2hsYmtabGRHTm9Bd0FBQUFBQUFzZjBGbFZLZERSeFJXZEdVazVYVmpSdGJsWXpNWGQ1VjBFQUFBQUFBQTRyTkJadVdWWXllV0ZCV2xORFF6aHNRM1JCZDBkRFoxQkJBQUFBQUFBT0t6VVdibGxXTW5saFFWcFRRME00YkVOMFFYZEhRMmRRUVE9PSIsImV4cCI6MTYwMTI5Mzk0OCwiaWF0IjoxNjAxMjkzNzY4fQ.CmuujiNhY8zzRtRnlpOsgplbyNpPqWjtVtf-dIvD6PLsJuhIwS5ETHGQW_KK4mcS

ListIdentifiers
https://oai.memobase.k8s.unibas.ch/?verb=ListIdentifiers&metadataPrefix=1234
mit anschliessendem resumptionToken - maximal 2 Minuten Zeitdauer zwischen den Aufrufen
aktuell Fehler!
https://oai.memobase.k8s.unibas.ch/?verb=ListIdentifiers&resumptionToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzM4NCJ9.eyJpc3MiOiJtZW1vYmFzZSBPQUkgc2VydmVyIiwic3ViIjoiRG5GMVpYSjVWR2hsYmtabGRHTm9Bd0FBQUFBQUFFeWFGbHAyZVVwb2FXWnpVVVJUTUdSdGVUbDZiSE4wY2tFQUFBQUFBQTRyZVJadVdWWXllV0ZCV2xORFF6aHNRM1JCZDBkRFoxQkJBQUFBQUFBT0szb1dibGxXTW5saFFWcFRRME00YkVOMFFYZEhRMmRRUVE9PSIsImV4cCI6MTYwMTI5NDE2NywiaWF0IjoxNjAxMjkzOTg3fQ.6dKO-1VRY4gybSBiC-wpUNd8SHFqiBAkv1YqG5w9AJ-hQz6AnN4UkHLwRKX1tjN4

ListSets
https://oai.memobase.k8s.unibas.ch/?verb=ListSets

Identify
https://oai.memobase.k8s.unibas.ch/?verb=Identify

Probleme:
- nicht gültige Parameter werden intern zwar geprüft, die Anzeige funktioniert bisher aber noch nicht
- ListIdentifier mit ResumptionToken
- from / until: bisher nicht implementiert, da wir noch kein passendes Datenmodell haben, welches diese Informationen enthalten würde

