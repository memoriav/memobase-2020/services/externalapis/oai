/*
I think we do not need it any more...
*/


```scala
object XmlCreator {

//todo: check if e.g. scalatags might be the better alternative
private val pRepName = "#REP_NAME#".r
private val pBaseUrl = "#BASE_URL#".r
private val pProtVersion = "#PROT_VERSION#".r
private val pEarliest = "#EARLIEST_DATESTAMP#".r
private val pEmail = "#ADMIN_EMAIL#".r
private val pDeleted = "#DELETED#".r
private val pGranularity = "#GRANULARITY#".r


def createIdentifyFragment(rep:OaiRepository): String = {

    pRepName.replaceAllIn(
        pBaseUrl.replaceAllIn(
          pProtVersion.replaceAllIn(
            pEarliest.replaceAllIn(
              pEmail.replaceAllIn(
                pDeleted.replaceAllIn(
                  pGranularity.replaceAllIn(
                    scala.io.Source.fromResource("resources/identify.xml").getLines().mkString,
                    rep.oaiConfig.identify.granularity
                  ),rep.oaiConfig.identify.deletedRecord)
              ,rep.oaiConfig.identify.adminEmail),
              rep.oaiConfig.identify.earliestDatestamp),
          rep.oaiConfig.identify.protocolVersion)
        ,rep.oaiConfig.identify.baseUrl),
      rep.oaiConfig.identify.repositoryName)
}

}

```