

#### korrekte Datumsprüfungen
das Datum muss Protokollgerecht behandelt werden
http://www.openarchives.org/OAI/openarchivesprotocol.html#Dates
Validation Komponente prüft bisher nur, ob die erforderlichen Parameter für die verben vorhanden sind
beschäftige Dich mit Java > v8 Zeit implementierung Joda typen 

Fehlerresponse DNB wenn ein Dokument mit GetRecord nicht gefunden werden kann

```xml 
<!-- DNB -->

<OAI-PMH xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
<responseDate>2021-03-17T08:56:51Z</responseDate>
<request metadataPrefix="MARC21-xml" verb="GetRecord" identifier="oai:dnb.de/authorities/11854023">https://services.dnb.de/oai/repository</request>
<error code="idDoesNotExist"/>
</OAI-PMH>

<!-- memobase OAI -->

<OAI-PMH xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
<responseDate>2021-03-17T08:29:24.416675Z</responseDate>
<request verb="GetRecord">https://oai.memobase.k8s.unibas.ch/</request>
<error code="badArgument">Illegal argument metadataPrefi in request</error>
<error code="badArgument">Illegal argument identifie in request</error>
</OAI-PMH>

<!-- Frage: doppelte error code erlaubt nach Standard? -->

<!-- was man noch machen sollte -->
<!-- Angabe aller request parameter - wie bei DNB - nach Standard mandatory? -->


```

##### noch zum Datum
wir unterstützen feinere Granularität als YYYY-MM-DD (Mindeststandard)
die Suche muss das jedoch korrekt unterstützen - Validierung und Tests erforderlich

#### Deletes
bis jetzt keine Lösung

#### sets 
aktuell nutze ich ein Pseudoset "memobaseset" - müssen wir klären


## details

#### resumption token bei ListIdentifiers hat noch einen Option typ
Some(ResumptionToken(memobase OAI server,1615980114,1615980294,DnF1ZXJ5VGhlbkZldGNoAwAAAAAAEoVQFm5ZVjJ5YUFaU0NDOGxDdEF3R0NnUEEAAAAAAAHr5RZVSnQ0cUVnRlJOV1Y0bW5WMzF3eVdBAAAAAAAB6-YWVUp0NHFFZ0ZSTldWNG1uVjMxd3lXQQ==,pdi.jwt.JwtClaim@eea84345,eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzM4NCJ9.eyJpc3MiOiJtZW1vYmFzZSBPQUkgc2VydmVyIiwic3ViIjoiRG5GMVpYSjVWR2hsYmtabGRHTm9Bd0FBQUFBQUVvVlFGbTVaVmpKNVlVRmFVME5ET0d4RGRFRjNSME5uVUVFQUFBQUFBQUhyNVJaVlNuUTBjVVZuUmxKT1YxWTBiVzVXTXpGM2VWZEJBQUFBQUFBQjYtWVdWVXAwTkhGRlowWlNUbGRXTkcxdVZqTXhkM2xYUVE9PSIsImV4cCI6MTYxNTk4MDI5NCwiaWF0IjoxNjE1OTgwMTE0fQ.6RzFQuBUorsdzQtcluNJpqU6daxGPS3ix8rjXUdZ3AA3ZsI8uT_H_JK--0mZJPoP))
bei allen anderen verben auch?
nein - bei ListRecords nicht

#### konfigurierbare Listenlängen

#### identifier
wie gehen wir damit um, welche nehmen wir (mit einem URI Anteil oder ohne)

#### chrome browser 
Problem mit Zeichenencoding

#### Umbrüche in oai response tags entfernen
u.a.
```xml
<identifier>
https://memobase.ch/record/srf-012-5E7F3070-33C4-4B5B-A985-1DB7708E4C40_07
</identifier>
```


#### set frage
Zuletzt noch eine Frage zu OAI - sicher nichts drigendes: 

In setSpec wird mir bei Requests mit set= jeweils nur das Set angezeigt, das ich 
abgerufen habe. Bei GetRecord kommt immer memobase. 

Ist das korrekt nach OAI? Ich habe von anderen Schnittstellen im Kopf, dass hier 
immer alle Sets angezeigt werden, in denen der Record enthalten ist.

@Silvia: ja - ist mir auch schon durch den Kopf. Ich muss den Standard nochmals genauer lesen, ob dieser sich dazu genauer äussert. 
Falls die Angabe "aller" sets notwendig ist, muss ich mir noch überlegen, wie ich das abbilden kann. 
Evtl. reicht die bestehende Konfiguration aus und es könnte eine Implementierung geben, die sich dessen bedient. 
Das muss ich mir aber noch durch den Kopf gehen lassen. 


