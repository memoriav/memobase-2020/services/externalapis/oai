import sbt.Keys._
import Dependencies._
//import play.sbt.PlaySettings


ThisBuild / scalaVersion := "2.13.14"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "memobase"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") Some(tag)
  else None
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning, PlayService, PlayLayoutPlugin, Common)
  .settings(
    name := "oaiServiceMemobase",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    /*
    assembly / assemblyMergeStrategy := {
      case "log4j.properties" => MergeStrategy.first
      case "module-info.class" => MergeStrategy.discard
      case "log4j2.xml" => MergeStrategy.first
      case PathList("play","reference-overrides.conf") => MergeStrategy.discard
      case PathList("org", "apache", "commons", "logging", "impl", "SimpleLog.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "impl", "SimpleLog$1.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "impl", "NoOpLog.class") => MergeStrategy.last
      case PathList("apache", "commons", "logging", "LogFactory.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "LogFactory.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "LogConfigurationException.class") => MergeStrategy.last
      case PathList("org", "apache", "commons", "logging", "Log.class") => MergeStrategy.last
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)

    },
*/

    //assembly / assemblyJarName := "app.jar",
    //assembly / test := {},
    //assembly / mainClass := Some("Main"),
    resolvers ++= Seq(
      //"Memobase Utils" at "https://dl.bintray.com/jonas-waeber/memobase"
      "Typesafe" at "https://repo.typesafe.com/typesafe/releases/",
      Resolver.mavenLocal
    ),
    libraryDependencies ++= Seq(
      guice,
      joda,
      lemonlabs,
      codingwell,
      scalatestplusplay % Test,
      elasticsearch,
      logging,
      json2xml,
      scala_xml_module,
      upickle,
      httprequests,
      jwtcore,
      saxonhe
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )

  )

PlayKeys.devSettings := Seq("play.server.http.port" -> "9000")

import com.typesafe.sbt.packager.MappingsHelper._
Universal / mappings ++= directory(baseDirectory.value / "resources")

