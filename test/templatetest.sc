val template = """<Identify>
  <repositoryName>#REP_NAME#</repositoryName>
  <baseURL>#BASE_URL#</baseURL>
  <protocolVersion>#PROT_VERSION#</protocolVersion>
  <adminEmail>#ADMIN_EMAIL#</adminEmail>
  <earliestDatestamp>#EARLIEST_DATESTAMP#</earliestDatestamp>
  <deletedRecord>#DELETED#</deletedRecord>
  <granularity>#GRANULARITY#</granularity>
</Identify>"""

val data = Map("#REP_NAME#" -> "aaa",
  "placeholder2" -> "xxx",
  "placeholder3" -> "yyy",
  "placeholder4" -> "zzz",
  "placeholder5" -> "bbb")

data.foldLeft(template){ case (newState, kv) => newState.replace(s"{${kv._1}}", kv._2)}