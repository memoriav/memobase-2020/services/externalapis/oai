/*
 * OAI Server Interface
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package org.swissbib.memobase.oai.common.validation

import org.scalatestplus.play.PlaySpec

class ContentErrorCodeSpec extends PlaySpec {

  /* test this later especially with PlaySpec - actually no time ...
  "error code enum" must {
    ("must be correct") must {

      val idNotExist = OAIErrorCode.ID_DOES_NOT_EXIST
      println(idNotExist.code)

      val ee = OAIErrorCode.getErrorCode(Some("noRecordsMatch"))
      println(ee)

    }

  }

   */

}
