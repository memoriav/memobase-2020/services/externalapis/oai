FROM eclipse-temurin:22-jre-alpine
WORKDIR /app
COPY target/app /app/
CMD ./start.process.sh
