# memobase OAI Server implementation

### Purpose
* provide standard general OAI server for memobase OAI datasets
* the configuration is optimized to fulfill memobase requirements for Europeana exports
* more specific configurations for other workflows have to be defined and implemented

### Implementation details
* Scala
* [Play framework](https://www.playframework.com/)
* modularized typed data repositories (for memobase we use an ElasticSearch data repository)
* alternatives could be Solr, Mongo,maybe Kafka, ... . These have to be implemented (via typed interfaces) if desired
