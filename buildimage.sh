#!/usr/bin/env bash

sbt clean
sbt stage

docker build -t "oaiservice" .
