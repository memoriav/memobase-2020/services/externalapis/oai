/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



package controllers

import controllers.BindController.AgeRange
import javax.inject.Inject
import play.api.Configuration
import play.api.mvc.{AbstractController, ControllerComponents, QueryStringBindable, Rendering}



class BindController @Inject()(cc: ControllerComponents,
                               config: Configuration
                               //kafka:KafkaComponent
                              ) extends AbstractController(cc) with Rendering {

  def age(age:AgeRange) = Action {
    Ok(age.from.asInstanceOf[String])
  }

}


object BindController  {
  case class AgeRange(from: String, to: String) {}


  implicit def queryStringBindable(implicit intBinder: QueryStringBindable[String]): QueryStringBindable[AgeRange] = new QueryStringBindable[AgeRange] {
    override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, AgeRange]] = {
      for {
        from <- intBinder.bind("from", params)
        to <- intBinder.bind("to", params)
      } yield {
        (from, to) match {
          case (Right(from), Right(to)) => Right(AgeRange(from, to))
          case _ => Left("Unable to bind an AgeRange")
        }
      }
    }
    override def unbind(key: String, ageRange: AgeRange): String = {
      intBinder.unbind("from", ageRange.from) + "&" + intBinder.unbind("to", ageRange.to)
    }
  }
}