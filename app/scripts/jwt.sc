import java.time.{Clock, ZoneId}

import com.fasterxml.jackson.databind.JsonNode
import pdi.jwt.{Jwt, JwtAlgorithm, JwtClaim, JwtHeader, JwtOptions}
import org.joda.time.{DateTime, Duration, Instant, LocalDateTime}
import play.api.libs.json.Json
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

import scala.util.{Failure, Success}

val token = Jwt.encode("""{"user":1}""", "secretKey", JwtAlgorithm.HS256)
implicit val clock: Clock = Clock.system(ZoneId.of("Europe/Zurich"))

val jwtClaim = JwtClaim(

  subject = Option("DnF1ZXJ5VGhlbkZldGNoAwAAAAAAAShoFlVKdDRxRWdGUk5XVjRtblYzMXd5V0EAAAAAAAYJKRZuWVYyeWFBWlNDQzhsQ3RBd0dDZ1BBAAAAAAAGCSgWbllWMnlhQVpTQ0M4bEN0QXdHQ2dQQQ=="),
  issuer = Option("memobase OAI server")
).issuedNow.expiresIn(10)

val key = "swortfish"
//val tokenWithClaim = Jwt.encode(jwtClaim,key,JwtAlgorithm.HS384)
val tokenWithClaim = Jwt.encode(JwtHeader(JwtAlgorithm.HS384), jwtClaim, key)

//Thread.sleep(3000)

//Jwt.decode(tokenWithClaim,key)
//val decoded = Jwt.decodeAll(tokenWithClaim, "swortfish", Seq(JwtAlgorithm.HS384))
val decoded = Jwt.decodeRaw(tokenWithClaim, "swortfish", Seq(JwtAlgorithm.HS384))



decoded match {
  case Success(value) =>
    //val j = JwtClaim(value)
    //println(j.subject)
    //val (header,claim,string) =   decoded.get
    //println("is valid: " + Jwt.isValid(tokenWithClaim))
    val jsnonNode: JsValue = Json.parse(value)
    val issuer = (jsnonNode \ "iss").as[String]
    val subject = (jsnonNode \ "sub").as[String]


    println(issuer)
    println(subject)
    //println(decoded)
    println(jsnonNode)

  case Failure(exception) => println(exception)
}



