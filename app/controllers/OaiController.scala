/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package controllers

import javax.inject.Inject
import modules.OaiRepository
import org.swissbib.memobase.oai.common.validation.OaiQueryParameterChecker
import org.swissbib.memobase.oai.runner.OaiRunner
import play.api.Configuration
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents, Rendering}

class OaiController @Inject()(cc: ControllerComponents)
                             (implicit config: Configuration,repository:OaiRepository ) extends AbstractController(cc) with Rendering {

  //todo: can we make the Action more type specific (not AnyContent)
  def handleRequest(verb: Option[String],
                    metadataPrefix: Option[String],
                    set: Option[String],
                    from:Option[String],
                    until:Option[String],
                    identifier: Option[String],
                    resumptionToken: Option[String]): Action[AnyContent] =
    Action { implicit request =>


      val qP: Map[String, Seq[String]] = request.queryString
      val runner: OaiRunner = new OaiQueryParameterChecker(
        verb = verb,
        metadataPrefix = metadataPrefix,
        set = set,
        from = from,
        until = until,
        identifier = identifier,
        resumptionToken = resumptionToken,
        allQueryParameter = qP,
        configuration = repository.oaiConfig).checkQueryParameter

      val response = runner.run().createResponse.toString()

      Ok(response).as("text/xml")
    }

  def fallback: Action[AnyContent] =
    Action { implicit request =>
      Ok(s"hello from fallback")
    }
}