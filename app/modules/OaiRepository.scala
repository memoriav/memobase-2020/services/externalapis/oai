/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package modules

import org.swissbib.memobase.oai.common.util.{OaiConfig, ResumptionToken}
import org.swissbib.memobase.oai.runner.ResultList

import scala.util.Try



case class OAIContent(esId: String, docId: String, document: String, format: String, published: Boolean,set: List[String], institution: List[String], updateDate: String )


trait OaiRepository {
  val oaiConfig: OaiConfig
  def listRecords(from: Option[String],
                  until: Option[String],
                 set: Option[String],
                 resumptionToken: Option[ResumptionToken],
                 metadataPrefix: String): Try[ResultList]



  def listIdentiers(from: Option[String],
                  until: Option[String],
                  set: Option[String],
                  resumptionToken: Option[ResumptionToken],
                  metadataPrefix: String): Try[ResultList]

  def getRecord(identifier: String,
               metadataPrefix: String
               ): Try[OAIContent]

  def listLength: Int

  def resumptionTokenTTL: Long





}
