/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package modules

import com.google.inject.AbstractModule
import javax.inject._
import net.codingwell.scalaguice.ScalaModule
import play.api.{Configuration, Environment}

/**
  * Sets up custom components for Play.
  *
  * https://www.playframework.com/documentation/latest/ScalaDependencyInjection
  */
class KafkaModule(environment: Environment, configuration: Configuration)
    extends AbstractModule
    with ScalaModule {

  //todo: look at the differences between instantiation of Elasticsearch module compared to the Kafka way here
  override def configure(): Unit = {
    //bind[PostRepository].to[PostRepositoryImpl].in[Singleton]

    //bind[SolrComponent].to[SolrComponentImpl].in[Singleton]

    //val t1 = environment
    //val t2 = configuration
    //bind[KafkaComponent].to[KafkaComponentImpl].in[Singleton]
  }
}

/*
Fehler in 2.13.3
[info] Non-compiled module 'compiler-bridge_2.13' for Scala 2.13.3. Compiling...
[info]   Compilation completed in 8.085s.
[error] /home/swissbib/environment/code/repositories/memoriav/gitlab/services/externalapis/oai/app/modules/KafkaModule.scala:25:51: Auto-application to `()` is deprecated. Supply the empty argument list `()` explicitly to invoke method in,
[error] or remove the empty argument list from its definition (Java-defined methods are exempt).
[error] In Scala 3, an unapplied method like this will be eta-expanded into a function.
[error]     bind[KafkaComponent].to[KafkaComponentImpl].in[Singleton]


 */
