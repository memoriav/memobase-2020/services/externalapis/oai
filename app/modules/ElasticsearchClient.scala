/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



package modules

import com.typesafe.config.{Config, ConfigValue, ConfigValueFactory}

import javax.inject.{Inject, Singleton}
import org.apache.http.{Header, HttpHost}
import org.apache.http.message.BasicHeader
import org.elasticsearch.client.{RestClient, RestHighLevelClient}
import org.swissbib.memobase.oai.common.util.{OAIMetadataPrefix, OAISet, OaiCommonConfig, OaiConfig, OaiConfigMetadataPrefixes, OaiConfigSets, OaiIdentifyConfig}
import play.Environment
import play.api.inject.ApplicationLifecycle

import java.util
import scala.jdk.CollectionConverters._
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future

@Singleton
class ElasticsearchClient @Inject()(
                              lifecycle: ApplicationLifecycle,
                              val config: Config,
                              val env: Environment
                            ) extends ElasticsearchComponent {
  lifecycle.addStopHook(() => {
    //Future.successful(client.get.close())
    Future.successful(client.getOrElse(Option.empty))
  })

  override val index: String = getEnv("ELASTIC_INDEX")

  //val client: Option[RestHighLevelClient] = connect()
  override val client: Option[RestHighLevelClient] = connect()

  //val oai = loadOaiConfig()

  override def listLength: Int = getEnv("RESPONSE_LISTLENGTH").toIntOption.getOrElse(30)

  override def resumptionTokenTTL: Long = getEnv("RESUMPTION_TOKEN_TTL").toLongOption.getOrElse(3)

  override val oaiConfig: OaiConfig = loadOaiConfig()
  //uploadTemplates()

  private def loadOaiConfig(): OaiConfig = {

    val sI = config.getObject("oaiconfigs.identify").toConfig
          //todo: make this as implicit Conversion
    val identifyConfig = OaiIdentifyConfig(
          earliestDatestamp = sI.getString("earliestDatestamp"),
          deletedRecord = sI.getString("deletedRecord"),
          adminEmail = sI.getString("adminEmail"),
          repositoryName = sI.getString("repositoryName"),
          baseUrl = sI.getString("baseURL"),
          granularity = sI.getString("granularity"),
          protocolVersion = sI.getString("protocolVersion"))

    val sC = config.getObject("oaiconfigs.common").toConfig
    val commonConfig = OaiCommonConfig(
      xsi_schemaLocation = sC.getString("xsi_schemaLocation")
    )


    //collection conversions
    //https://stackoverflow.com/questions/8301947/what-is-the-difference-between-javaconverters-and-javaconversions-in-scala
    val oaiSets:Map[String,OAISet] = config.getObjectList("oaiconfigs.sets").asScala.toSeq.map(
      o => {
        o.get("spec").unwrapped().toString -> OAISet(
        spec = o.get("spec").unwrapped().toString,
        name = o.get("name").unwrapped().toString,
        includedSets = Option(o.getOrDefault("includedsets",null))
          .map(_.unwrapped().asInstanceOf[util.ArrayList[String]].asScala.toList),
        field = Option(o.getOrDefault("field",null)).map(_.unwrapped().toString),
        fieldValue = Option(o.getOrDefault("value",null)).map(_.unwrapped().toString)
        )
      }
    ).toMap
    val oaiConfigPrefixes = config.getObjectList("oaiconfigs.metadataPrefix").asScala.toSeq

    val oaiPrefixes = config.getObjectList("oaiconfigs.metadataPrefix").asScala.toSeq.map(
      o =>
        OAIMetadataPrefix (metadataPrefix = o.get("metadataPrefix").unwrapped().toString,
          namespace = o.get("metadataNamespace").unwrapped().toString,
          schema = o.get("schema").unwrapped().toString)
    )



    OaiConfig(
      commonConfig,
      identifyConfig,
      oaiSets,
      oaiPrefixes.toList )

  }

  private def connect(): Option[RestHighLevelClient] = {
    val hosts = new ArrayBuffer[HttpHost]
    val configuredHosts = getEnv("ELASTIC_HOST").split(";")
    val configuredPort = getEnv("ELASTIC_PORT").toIntOption.getOrElse(8080)
    configuredHosts.foreach(
      value => {
        hosts += new HttpHost(value, configuredPort)
      }
    )
    //val headers = Array(new BasicHeader("cluster.name", getEnv("ELASTIC_CLUSTERNAME"))
    //  .asInstanceOf[Header])
    Option(new RestHighLevelClient(RestClient.builder(hosts.toArray : _*)))
  }

  private def getEnv(value: String): String = {
    val envValue: Option[String] = sys.env.get(value)
    if (envValue.isDefined) {
      envValue.get
    } else {
      Option(System.getProperties.get(value)) match {
        case Some(value) => value.toString
        case None => throw new Exception("Environment variable " + value + " not available")
      }
    }
  }


}
