/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package modules

import java.util
import com.typesafe.config.Config
import org.elasticsearch.action.get.GetRequest
import org.elasticsearch.action.search.{SearchRequest, SearchResponse, SearchScrollRequest}
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.index.query.{QueryBuilder, QueryBuilders, TermQueryBuilder}
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.swissbib.memobase.oai.common.util.es.QueryBuilderHelper
import org.swissbib.memobase.oai.common.util.{ESResumptionTokenHelper, OaiConfig, ResumptionToken}
import org.swissbib.memobase.oai.runner.{GetRecordFailure, ResultList}
import play.Environment
import utilities.Helper

import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success, Try}

//noinspection ScalaStyle
trait ElasticsearchComponent extends OaiRepository {
  val client: Option[RestHighLevelClient]

  val config: Config
  val env: Environment
  //to do: another solution necessary in case  we decide to use several indices for OAI
  val index: String

  override def listRecords(from: Option[String],
                           until: Option[String],
                           set: Option[String],
                           resumptionToken: Option[ResumptionToken],
                           metadataPrefix: String): Try[ResultList] = Try{
    //todo - more / better / tested handling for different parameters

    val searchResponse:Try[SearchResponse] = (from, until, set, resumptionToken, metadataPrefix) match {
      case (Some(from), Some(until),set,None,metaData) =>

        val queries = if (set.isDefined) {
          createSetBoolQuery(set.get,oaiConfig).must(from_untilRangeQuery(from,until))
        } else {
          from_untilRangeQuery(from,until)
        }

        val searchRequest = createScrollSearchRequest(metaData,queries)
        Try {client.get.search(searchRequest, RequestOptions.DEFAULT)}

      case (Some(from), None ,set,None,metaDataP) =>

        val queries = if (set.isDefined) {
          createSetBoolQuery(set.get,oaiConfig).must(fromRangeQuery(from))
        } else {
          fromRangeQuery(from)
        }

        val searchRequest = createScrollSearchRequest(metaDataP,queries)
        Try {client.get.search(searchRequest, RequestOptions.DEFAULT)}

      case (None, Some(until) ,set,None,metaDataP) =>

        val queries = if (set.isDefined) {
          createSetBoolQuery(set.get,oaiConfig).must(untilRangeQuery(until))
        } else {
          untilRangeQuery(until)
        }

        val searchRequest = createScrollSearchRequest(metaDataP,queries)
        Try {client.get.search(searchRequest, RequestOptions.DEFAULT)}


      case (_, _,_,Some(resumptionToken),_) =>
        //use only the resumption token
        val scrollRequest = new SearchScrollRequest(resumptionToken.subject)
        scrollRequest.scroll(TimeValue.timeValueMinutes(3L))
        Try[SearchResponse] {client.get.scroll(scrollRequest, RequestOptions.DEFAULT)}
      //only for the moment until we have better data
      case (_, _,set,_,metadataP) =>

        val queries = if (set.isDefined) {
          createSetBoolQuery(set.get,oaiConfig)
        } else {
           QueryBuilders.matchAllQuery()
        }

        val searchRequest = createScrollSearchRequest(metadataP,queries)
        Try {client.get.search(searchRequest, RequestOptions.DEFAULT)}

    }


    searchResponse match  {
      case Success(searchResponse) =>

        //scroll_id is always the same -> for this specific context
        //condition to finish the fetching is to compare the length of the resultlist
        //compare: https://www.elastic.co/guide/en/elasticsearch/client/java-api/current/java-search-scrolling.html
        //otherwise we will get into an empty loop
        val scrollId = if (searchResponse.getHits.getHits.length < listLength) None else Option(searchResponse.getScrollId)
        val resumptionToken: Option[ResumptionToken] = scrollId.map(
          ESResumptionTokenHelper(_))

        val contentList = searchResponse.getHits.getHits.
          map(hit => {
            val source = hit.getSourceAsMap.asScala

            OAIContent(hit.getId,
              source.getOrElse("id","").toString,
              if (source.contains("document")) (Helper.decodeBase64FromString(source("document").toString)) else "",
              //source.getOrElse("document","").toString,
              source.getOrElse("format","").toString,
              source.getOrElse("published",false).asInstanceOf[Boolean],
              source.getOrElse("recordset",new util.ArrayList[String]()).asInstanceOf[util.ArrayList[String]].asScala.toList,
              source.getOrElse("institution",new util.ArrayList[String]()).asInstanceOf[util.ArrayList[String]].asScala.toList,
              source.getOrElse("lastUpdatedDate", "").asInstanceOf[String])

          }
          ).toSeq


        ResultList(resumptionToken, contentList)
      case Failure(exception) => throw exception
    }

  }



  override def listIdentiers(from: Option[String],
                             until: Option[String],
                             set: Option[String],
                             resumptionToken: Option[ResumptionToken],
                             metadataPrefix: String): Try[ResultList] =
    listRecords(from,
                until,
                set,
                resumptionToken,
                metadataPrefix)


  override def getRecord(identifier: String,
                         metadataPrefix: String): Try[OAIContent] = Try {

    val getRequest = new GetRequest(index, identifier)
    val getResponse = client.get.get(getRequest, RequestOptions.DEFAULT)


    //todo: by now metadataPrefix is not being considered although it's mandatory in the OAI standard
    // we provide any legal format for any document
    // should be changed ones we differentiate in more detail
    if (getResponse.isSourceEmpty) {
      throw GetRecordFailure("record was not found")
    } else {
      val hit = getResponse.getSource.asScala
      getResponse.getId
      OAIContent(getResponse.getId,
        hit.getOrElse("id", "").toString,
        //todo: test it
        if (hit.contains("document")) (Helper.decodeBase64FromString(hit("document").toString)) else "",
        //hit.getOrElse("document", "").toString,
        hit.getOrElse("format", "").toString,
        hit.getOrElse("published", false).asInstanceOf[Boolean],
        hit.getOrElse("recordset", new util.ArrayList[String]()).asInstanceOf[util.ArrayList[String]].asScala.toList,
        hit.getOrElse("institution", new util.ArrayList[String]()).asInstanceOf[util.ArrayList[String]].asScala.toList,
        hit.getOrElse("lastUpdatedDate", "").asInstanceOf[String]
      )

    }

  }

  private def createSetBoolQuery(set: String, oaiConfig: OaiConfig) = {
    val queries: util.List[TermQueryBuilder] = QueryBuilderHelper.setSpecMatchQuery(set,oaiConfig).asJava
    val boolQueryBuilder = QueryBuilders.boolQuery()

    //todo: if we avoid throwing an exception setSpecMatchQuery
    // use foldLeft but test if list is empty. In this case create matchAll (or throw an exception??)
    queries.forEach(q =>  boolQueryBuilder.should(q))
    boolQueryBuilder

  }

  private def from_untilRangeQuery(from: String, until: String) =
    QueryBuilders
      .rangeQuery("lastUpdatedDate")
      .gte(from).format("strict_date_time")
      .lte(until).format("strict_date_time")

  private def fromRangeQuery(from: String) =
    QueryBuilders
      .rangeQuery("lastUpdatedDate")
      .gte(from).format("strict_date_time")


  private def untilRangeQuery(until: String) =
    QueryBuilders
      .rangeQuery("lastUpdatedDate")
      .lte(until).format("strict_date_time")


  private def composeMetadataLeaf(metaDataPrefix: String, qb: QueryBuilder) =
    QueryBuilders.boolQuery()
      .must(qb)
      .must(QueryBuilders.termQuery("format",metaDataPrefix))

  private def createScrollSearchRequest(metaDataPrefix: String, queries: QueryBuilder) = {
    val searchSourceBuilder = new SearchSourceBuilder()
      .query(composeMetadataLeaf(metaDataPrefix,queries)).size(listLength)
    //println(searchSourceBuilder.toString)

    new SearchRequest().source(searchSourceBuilder)
      .indices(index).scroll(TimeValue.timeValueMinutes(resumptionTokenTTL))

  }


}
