/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package utilities

import java.io.ByteArrayOutputStream
import java.util.Base64
import java.util.zip.Inflater

object Helper {

  def deserialize(data: Array[Byte]): String = {
    val decompressor = new Inflater()
    decompressor.setInput(data)
    val bos = new ByteArrayOutputStream(data.length)
    val buffer = new Array[Byte](8192)
    while (!decompressor.finished) {

      val size = decompressor.inflate(buffer)
      bos.write(buffer, 0, size)
    }
    bos.toString

  }

  def decodeBase64FromString(encodedBase64String: String): String = {
    deserialize(Base64.getDecoder.decode(encodedBase64String))
  }

}
