/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package utilities

import java.io.{ByteArrayInputStream, StringWriter}
import java.nio.charset.Charset

import de.odysseus.staxon.json.{JsonXMLConfigBuilder, JsonXMLInputFactory}
import de.odysseus.staxon.xml.util.PrettyXMLEventWriter
import javax.xml.stream.XMLOutputFactory
import play.api.libs.json.{JsObject, JsValue}

import scala.xml.{Elem, Node}

object Json2XML {

  def singleJsonDoc2Xml(doc: JsValue, root: String = "record"): Elem = {

    val esJsonRespone = JsObject(Seq(root -> doc)).toString
    val config = new JsonXMLConfigBuilder().multiplePI(false).build();
    val output = new StringWriter()

    val reader = new JsonXMLInputFactory(config).createXMLEventReader(
      new ByteArrayInputStream(esJsonRespone.getBytes(Charset.forName("UTF-8")))
    )
    val writer = XMLOutputFactory.newInstance().createXMLEventWriter(output)
    val prettyWriter = new PrettyXMLEventWriter(writer)

    prettyWriter.add(reader)
    reader.close()
    writer.close()


    //at least the record tag of the metadata section for each document needs a namespace.
    //otherwise standard OAI client tools are not able to process the result
    scala.xml.XML.loadString(new TemplateTransformer(output.toString).transform(XSLTWrapper.edmNamespaceTransformer))

  }

  def multipleJsonDoc2Xml(doc: JsValue, root: String = "record"): Seq[Node] = {

    Seq[Node]()

  }

}