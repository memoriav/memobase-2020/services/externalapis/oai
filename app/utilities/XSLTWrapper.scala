/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package utilities

import javax.xml.transform.Transformer

object XSLTWrapper {

  val TRANSFORMERIMPL = "net.sf.saxon.TransformerFactoryImpl"
  val EDM_namespace = "resources/recordnscreation.xsl"

  lazy val edmNamespaceTransformer: Transformer = {
    new TemplateCreator(TRANSFORMERIMPL, EDM_namespace).createTransformerFromResource
  }

  def templatefromString: String =
    """<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
      |                xmlns:fn="http://www.w3.org/2005/xpath-functions"
      |                exclude-result-prefixes="fn">
      |    <!--xmlns:fn="http://www.w3.org/2005/xpath-functions"> -->
      |
      |    <xsl:output method="xml"
      |                encoding="UTF-8"
      |                indent="yes"
      |                omit-xml-declaration="yes"
      |    />
      |
      |    <!--
      |    <xsl:attribute-set name="edm">
      |        <xsl:attribute name="edm">https://europeana.org/irgendwas</xsl:attribute>
      |    </xsl:attribute-set>
      |    -->
      |    <xsl:template match="record">
      |
      |        <xsl:element  namespace="https://europeana.org/irgendwas"  name="edm:{local-name()}">
      |
      |
      |            <xsl:apply-templates select="@*|node()"/>
      |        </xsl:element>
      |
      |
      |    </xsl:template>
      |
      |
      |    <xsl:template match="@*|node()">
      |        <xsl:copy-of select="."/>
      |    </xsl:template>
      |
      |</xsl:stylesheet>
      |
      |
      |""".stripMargin

}
