/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package utilities

import java.io.IOException
import java.nio.file.{Files, Paths}

import play.{Environment, Logger}

object FileUtil {

  def readFile(name: String, env: Environment): String = try {
    val file = env.getFile(name)
    val path = Paths.get(file.getAbsolutePath)
    String.join("\n", Files.readAllLines(path))
  } catch {
    case e: IOException =>

      //Logger.error is deprecated which gives compile error in case of settings for this
      println ("Could not load mustache template: " + name, e)

      //Logger.error("Could not load mustache template: " + name, e)
      ""
  }
}
