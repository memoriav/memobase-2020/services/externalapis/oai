<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                exclude-result-prefixes="fn">
    <!--xmlns:fn="http://www.w3.org/2005/xpath-functions"> -->

    <xsl:output method="xml"
                encoding="UTF-8"
                indent="yes"
                omit-xml-declaration="yes"
    />

    <!--
    <xsl:attribute-set name="edm">
        <xsl:attribute name="edm">https://europeana.org/irgendwas</xsl:attribute>
    </xsl:attribute-set>
    -->
    <xsl:template match="record">

        <xsl:element  namespace="https://europeana.org/irgendwas"  name="edm:{local-name()}">


            <xsl:apply-templates select="@*|node()"/>
        </xsl:element>


    </xsl:template>


    <xsl:template match="@*|node()">
        <xsl:copy-of select="."/>
    </xsl:template>

</xsl:stylesheet>

