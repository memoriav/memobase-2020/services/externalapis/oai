/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



package org.swissbib.memobase.oai.response

import java.time.Instant
import org.swissbib.memobase.oai.common.util.OaiConfig
import org.swissbib.memobase.oai.common.validation.OaiParameterBase
import scala.xml.{Elem, MetaData, Node, Null, Text, TopScope, UnprefixedAttribute}


abstract class OaiResponse (oaiConfig: OaiConfig
                                   ,parameter: OaiParameterBase) {

  def makeHeader(del: Boolean = false,

                 datestamp: Instant = Instant.now(),
                 identifier: String): Elem = {

    //seems to be standard that in all cases a setSpec is part of the header of a record
    //if the set isn't given (optional parameter) we make a fallback to the highest set in set hierarchy
    val set = parameter.allQueryParameter.getOrElse("set",List("memobase")).head
    <header status={if (del) Some(Text("deleted")) else None}>
      <identifier>{identifier}</identifier>
      <datestamp>{datestamp}</datestamp>
      <setSpec>{set}</setSpec>
    </header>
  }


  def createRequestTag: Elem = {

    //completely scala xml voodoo...
    //https://stackoverflow.com/questions/43751677/how-to-create-xml-attributes-dynamic-via-map-in-scala
    //take some time to understand this...
    val seed: MetaData = Null
    val meta: MetaData = parameter.allQueryParameter.toList.foldLeft(seed) {
      case (acc, (s1, s2)) =>
        new UnprefixedAttribute(
          //filter out characters used for multiple parameter
          key = s1.replaceAll("""[\[\]]""", ""),
          value = s2.mkString(","),
          next = acc
        )
    }
    //first try
    //<request verb={runner.request.parameter.verb.toString}>{runner.repository.oaiConfig.identify.baseUrl}</request>
    //noinspection ScalaStyle
    Elem(
      prefix = null,
      label = "request",
      attributes = meta,
      scope = TopScope,
      minimizeEmpty = false,
      child = Text(oaiConfig.identify.baseUrl)
    )
  }

  protected def createOaiFrame: Elem = {
    val oaiFrame =
    //noinspection ScalaStyle
      <OAI-PMH xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd" xmlns="http://www.openarchives.org/OAI/2.0/"  >
        <responseDate>{Instant.now()}</responseDate>
        {createRequestTag}
      </OAI-PMH>

    oaiFrame
  }

  def createResponse:Seq[Node]

  override def toString: String = s"Ich bin eine OAI response mit Header für : $getVerb} "

  //verb should always be available
  def getVerb:String = parameter.allQueryParameter("verb").head

}














