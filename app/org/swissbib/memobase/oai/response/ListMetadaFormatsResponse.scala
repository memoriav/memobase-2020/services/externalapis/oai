/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.response

import com.typesafe.config.ConfigObject
import modules.OaiRepository
import org.swissbib.memobase.oai.common.util.OAIMetadataPrefix
import org.swissbib.memobase.oai.common.validation.OaiParameterBase
import play.api.Configuration

import scala.xml.{Elem, Node}
import scala.xml.transform.{RewriteRule, RuleTransformer}

case class ListMetadaFormatsResponse( config: Configuration
                                      ,repository: OaiRepository
                                      ,parameter: OaiParameterBase) extends OaiResponse (repository.oaiConfig
                                                                                      ,parameter) {
  override def toString: String = s"Ich bin eine ListMetadaFormats response mit Header für : ListMetadaFormats "


  override def createResponse: Seq[Node] = {

    //todo create the body using the executed action in repository
    val oaiFrame = createOaiFrame

    val sets: Seq[OAIMetadataPrefix] = repository.oaiConfig.prefixes

    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "request" =>
          elem ++ <ListMetadataFormats>
            {sets.map(prefixDef =>
              <metadataFormat>
                <metadataPrefix>{prefixDef.metadataPrefix}</metadataPrefix>
                <schema>{prefixDef.schema}</schema>
                <metadataNamespace>{prefixDef.namespace}</metadataNamespace>
              </metadataFormat>
            )}
          </ListMetadataFormats>

        case n => n
      }
    }
    new RuleTransformer(verbBody).transform(oaiFrame)
  }
}
