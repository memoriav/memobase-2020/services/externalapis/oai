/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.response

import modules.OaiRepository
import org.swissbib.memobase.oai.common.validation.OaiParameterBase
import org.swissbib.memobase.oai.runner.ResultList
import play.api.Configuration

import java.time.Instant
import scala.xml.{Elem, Node, XML}
import scala.xml.transform.{RewriteRule, RuleTransformer}

case class ListRecordsResponse(config: Configuration
                               ,repository: OaiRepository
                               ,resultList: ResultList
                               ,parameter: OaiParameterBase) extends OaiResponse (repository.oaiConfig
                                                                                      ,parameter)  {
  override def toString: String = s"Ich bin eine ListRecords response mit Header für : ListRecords "


  override def createResponse: Seq[Node] = {

    //todo create the body using the executed action in repository

    val oaiFrame = createOaiFrame

    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "request" =>
          elem ++
            <ListRecords>
              {
              resultList.result.map(hit =>
                <record>
                  {makeHeader(identifier = hit.docId,
                  datestamp = Instant.parse(hit.updateDate)
                )}
                  <metadata>{XML.loadString(hit.document)}</metadata>
                </record>)

              }
              {
              /*
              why doesn't this work??
              runner.resultList.get.repositoryToken.map(
                tokenvalue =>
                  (<resumptionToken>{tokenvalue}</resumptionToken>).get
              )

               */
              {if (resultList.repositoryToken.isDefined) {
                <resumptionToken>{resultList.repositoryToken.get.token}</resumptionToken>
              }}
              }

            </ListRecords>
        case n => n
      }
    }
    new RuleTransformer(verbBody).transform(oaiFrame)
  }
}
