/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.response

import modules.OaiRepository
import org.swissbib.memobase.oai.common.validation.OaiParameterBase
import play.api.Configuration

import scala.xml.{Elem, Node}
import scala.xml.transform.{RewriteRule, RuleTransformer}

case class IdentifyResponse(config: Configuration
                            ,repository: OaiRepository
                            ,parameter: OaiParameterBase) extends OaiResponse (repository.oaiConfig
                                                                            ,parameter) {
  override def toString: String = s"Ich bin eine Identify response mit Header für : Identify "


  override def createResponse: Seq[Node] = {

    //Hinweise für xml transformation
    //https://stackoverflow.com/questions/2199040/scala-xml-building-adding-children-to-existing-nodes
    //https://stackoverflow.com/questions/7160298/scala-xml-parsing-constructingparser-splitting-text-content?rq=1

    //https://github.com/scala/scala-xml/wiki/Getting-started
    //https://github.com/lauris/awesome-scala#xml--html


    val oaiFrame = createOaiFrame

    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "request" =>
          elem ++  <Identify>
            <repositoryName>{repository.oaiConfig.identify.repositoryName}</repositoryName>
            <baseURL>{repository.oaiConfig.identify.baseUrl}</baseURL>
            <protocolVersion>{repository.oaiConfig.identify.protocolVersion}</protocolVersion>
            <adminEmail>{repository.oaiConfig.identify.adminEmail}</adminEmail>
            <earliestDatestamp>{repository.oaiConfig.identify.earliestDatestamp}</earliestDatestamp>
            <deletedRecord>{repository.oaiConfig.identify.deletedRecord}</deletedRecord>
            <granularity>{repository.oaiConfig.identify.granularity}</granularity>
          </Identify>
        case n => n
      }
    }

    new RuleTransformer(verbBody).transform(oaiFrame)
  }

}
