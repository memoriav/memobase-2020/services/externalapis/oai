/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.response

import com.typesafe.config.ConfigObject
import modules.OaiRepository
import org.swissbib.memobase.oai.common.util.OAISet
import org.swissbib.memobase.oai.common.validation.{BadArgumentsParameter, SystemErrorParameter}
import play.api.Configuration

import scala.collection.immutable
import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml.{Elem, Node}

case class SystemErrorResponse(config: Configuration
                               , repository: OaiRepository
                               , parameter: SystemErrorParameter) extends OaiResponse (repository.oaiConfig
                                                                                          ,parameter) {
  override def toString: String = s"Ich bin eine BadArguments response "


  override def createResponse: Seq[Node] = {

    //todo create the body using the executed action in repository
    val oaiFrame = createOaiFrame
    //val sets: Seq[OAISet] = repository.oaiConfig.sets.sets

    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "request" =>
          elem ++ {
                <error code={parameter.errorCode}>{parameter.errorMessage}</error>
          }

        case n => n
      }
    }
    new RuleTransformer(verbBody).transform(oaiFrame)

  }
}
