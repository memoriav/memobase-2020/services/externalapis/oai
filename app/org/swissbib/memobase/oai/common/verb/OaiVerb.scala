/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.common.verb

import org.swissbib.memobase.oai.common.validation.{MissingParameterException, MissingQueryParameter, SystemFailureRequestValidation, WrongQueryParameterValue}

import scala.util.{Failure, Success, Try}

object OaiVerb extends Enumeration {

  type OaiVerb = Value

  protected case class OaiVal(verb: String) extends super.Val {

    //https://www.scala-lang.org/api/current/scala/Enumeration.html
  }

  import scala.language.implicitConversions

  implicit def getVerb(x: Value): String = x.asInstanceOf[OaiVal].verb

  val ListRecords: OaiVal = OaiVal("ListRecords")
  val ListSets: OaiVal = OaiVal("ListSets")
  val ListIdentifiers: OaiVal = OaiVal("ListIdentifiers")
  val GetRecord: OaiVal = OaiVal("GetRecord")
  val ListMetadataFormats: OaiVal = OaiVal("ListMetadataFormats")
  val Identify: OaiVal = OaiVal("Identify")
  val WrongVerb: OaiVal = OaiVal("WrongVerb")
  val MissingVerb: OaiVal = OaiVal("MissingVerb")

  private val mv = ".*None\\.get".r

  def getVerb(verb: Option[String]): OaiVerb = {
      Try[OaiVerb] (OaiVerb.withName(verb.get)) match {
        case Success(value) =>
          //print(value)
          value
        case Failure(exception) => if (mv.matches(exception.getMessage)) MissingVerb else WrongVerb

      }
    }

}


