/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.common.util

import com.typesafe.config.ConfigObject

case class OaiConfig(common: OaiCommonConfig,
                     identify: OaiIdentifyConfig,
                     sets: Map[String,OAISet],
                     prefixes: List[OAIMetadataPrefix])



case class OaiCommonConfig(
                            xsi_schemaLocation: String
                          )

case class OaiIdentifyConfig(earliestDatestamp: String,
                             deletedRecord: String,
                             adminEmail: String,
                             repositoryName: String,
                             baseUrl: String,
                             granularity: String,
                             protocolVersion: String
                            )

case class OaiConfigSets(sets: Seq[OAISet])

case class OAISet(spec:String, name: String,includedSets:Option[List[String]], field:Option[String],
                  fieldValue: Option[String] )

case class OaiConfigMetadataPrefixes(prefixes: Seq[OAIMetadataPrefix])

case class OAIMetadataPrefix(metadataPrefix:String, namespace:String, schema: String)
