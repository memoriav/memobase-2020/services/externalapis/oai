/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.common.util.es


import org.swissbib.memobase.oai.common.util.OaiConfig
import org.elasticsearch.index.query.TermQueryBuilder


object QueryBuilderHelper {


  //@tailrec todo: make it tail recursive
  def setSpecMatchQuery(set: String, oaiConfig: OaiConfig): List[TermQueryBuilder] = {
    val setInQuery = oaiConfig.sets(set)
    if (setInQuery.field.isDefined && setInQuery.fieldValue.isDefined) {
      List(new TermQueryBuilder(setInQuery.field.get,setInQuery.fieldValue.get))
    } else if (setInQuery.includedSets.isDefined) {

      setInQuery.includedSets.get.foldLeft(
        List[List[TermQueryBuilder]]())((list, set) =>
        setSpecMatchQuery(set, oaiConfig) :: list).flatten

    } else {
      //todo: maybe an empty list would be more adequate
      throw new Exception("set definiton not consistent")
    }
  }



}
