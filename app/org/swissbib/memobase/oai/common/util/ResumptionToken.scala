/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.common.util
import java.time.{Clock, ZoneId}
import pdi.jwt.{Jwt, JwtAlgorithm, JwtClaim, JwtHeader}
import play.api.libs.json.Json
import play.api.libs.json._
import play.api.libs.json.Reads._

import scala.util.{Failure, Success, Try}



case class ResumptionToken (issuer: String, issuedAt: Long, expiration: Long, subject: String, claim: JwtClaim, token:String)

object ESResumptionTokenHelper {

  implicit val clock: Clock = Clock.system(ZoneId.of("Europe/Zurich"))
  def apply(esCursor: String, expiresInSeconds: Long = 180, key:String = "oaimemobase"): ResumptionToken = {
    val jwtClaim = JwtClaim(
      subject = Option(esCursor),
      issuer = Option("memobase OAI server")
    ).issuedNow.expiresIn(expiresInSeconds)
    val tokenWithClaim = Jwt.encode(JwtHeader(JwtAlgorithm.HS384), jwtClaim, key)

    ResumptionToken( jwtClaim.issuer.get,jwtClaim.issuedAt.get,jwtClaim.expiration.get,jwtClaim.subject.get,jwtClaim,tokenWithClaim)

  }

  def createOaiToken(rt: ResumptionToken, key: String="oaimemobase"): String = {
    Jwt.encode(JwtHeader(JwtAlgorithm.HS384), rt.claim, key)
  }

  def createResumptionTokenFromOaiToken(oaiToken: String, key:String="oaimemobase",expiresInSeconds: Long = 180): Try[ResumptionToken] = {
    val decoded = Jwt.decodeRaw(oaiToken, key, Seq(JwtAlgorithm.HS384))

    decoded match {
      case Success(value) =>
        val jsnonNode: JsValue = Json.parse(value)
        val issuer = (jsnonNode \ "iss").as[String]
        val subject = (jsnonNode \ "sub").as[String]
        val expiration = (jsnonNode \ "exp").as[Long]
        val issuedAt = (jsnonNode \ "iat").as[Long]

        val jwtClaim = JwtClaim(
          subject = Option(subject),
          issuer = Option("memobase OAI server")
        ).issuedAt(issuedAt).expiresIn(expiresInSeconds)
        val tokenWithClaim = Jwt.encode(JwtHeader(JwtAlgorithm.HS384), jwtClaim, key)


        Success(ResumptionToken(issuer,expiration,issuedAt,subject,jwtClaim,tokenWithClaim))

      case Failure(value) => Failure(value)
    }

  }



}

object Test extends App {

  val rt = ESResumptionTokenHelper("1234",5)
  val token = ESResumptionTokenHelper.createOaiToken(rt)

  //Thread.sleep(6000)
  ESResumptionTokenHelper.createResumptionTokenFromOaiToken(token) match {
    case Success(value) => println(value)
    case Failure(value) => println(value)
  }

}

