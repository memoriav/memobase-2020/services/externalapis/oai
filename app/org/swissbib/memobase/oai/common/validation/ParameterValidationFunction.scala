/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



package org.swissbib.memobase.oai.common.validation

import org.swissbib.memobase.oai.common.util.{OAISet, OaiConfig}
import org.swissbib.memobase.oai.runner.OaiRunner

private object AllowedParameter {
  val identify = List("verb")
  val listRecordResumption = List("verb","resumptionToken")
  val getRecord = List("verb","identifier","metadataPrefix")
  val listMetadataFormatsAllowed = List("verb","identifier")
  val listMetadataFormatsMandatory = List("verb")
  val listSetsAllowed = List("verb")
  val listIdentifiers = List("verb", "from", "until", "metadataPrefix", "set")
  val listIdentifiersResumption = List("verb","resumptionToken")
  val listIdentifiersAllAllowed = List("verb", "from", "until", "metadataPrefix", "set", "resumptionToken")
  val listRecordsAllAllowed = List("verb", "from", "until", "metadataPrefix", "set", "resumptionToken")

  /*
  **from** an optional argument with a UTCdatetime value, which specifies a lower bound for datestamp-based selective harvesting.
    **until** an optional argument with a UTCdatetime value, which specifies a upper bound for datestamp-based selective harvesting.
    **metadataPrefix** a required argument, which specifies that headers should be returned only if the metadata format matching the supplied metadataPrefix is available or, depending on the repository's support for deletions, has been deleted. The metadata formats supported by a repository and for a particular item can be retrieved using the ListMetadataFormats request.
  **set** an optional argument with a setSpec value , which specifies set criteria for selective harvesting.
  **resumptionToken** an exclusive argument with a value that is the flow control token returned by a previous ListIdentifiers request that issued an incomplete list.
  */

}



abstract class ParameterValidationFunction () extends Function1[OaiCheckedVerbWithParameter, OaiRunner] {

  def checkParameterByName(paramsInReq: Map[String, Seq[String]], allowedList:List[String]): Seq[String] = {
    paramsInReq.keySet.diff(allowedList.toSet).toSeq
  }

  def checkMissingParameterByName(allowedList:List[String], paramsInReq: Map[String, Seq[String]]): Seq[String] = {
    allowedList.toSet.diff(paramsInReq.keySet).toSeq
  }

  def checkValidSet(set: String, oaiConfig: OaiConfig): Boolean = oaiConfig.sets.contains(set)


  def checkValidMetadaPrefix(metadataPrefix: String, oaiConfig: OaiConfig): Boolean = oaiConfig.prefixes
    .exists(_.metadataPrefix == metadataPrefix)


  override def apply(v1: OaiCheckedVerbWithParameter): OaiRunner
}



