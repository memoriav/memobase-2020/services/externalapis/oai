/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.common.validation

import org.swissbib.memobase.oai.runner.{BadArgumentsErrorRunner, ListSetsRunner, OaiRunner}

/*
https://www.openarchives.org/OAI/openarchivesprotocol.html#ListSets
Arguments
resumptionToken an exclusive argument with a value that is the flow control token returned by a previous ListSets request that issued an incomplete list.
GH: guess we do not need this for memobase because there aren't so much sets - probably

Error and Exception Conditions
badArgument - The request includes illegal arguments or is missing required arguments.
badResumptionToken - The value of the resumptionToken argument is invalid or expired.
noSetHierarchy - The repository does not support sets.

 */


object ListSetsValidation extends ParameterValidationFunction {
  override def apply(v1: OaiCheckedVerbWithParameter): OaiRunner =
    v1 match {
      //possible valid combination
        //todo: wir haben noch den Fall ListSetsExclusiveRunner abgedeckt
        // spielt bei uns im Moment aber keine Rolle, da unsere Liste der sets noch beschränkt ist
        // es braucht dafür aber ein Konzept


      case OaiCheckedVerbWithParameter(Some(verb), _, _, _, _, _, _, allParameter, configuration) =>

        checkParameterByName(allParameter, AllowedParameter.listSetsAllowed) match {
          case Seq() => ListSetsRunner(CheckedListSetsParameter(verb, allParameter))
          case setWithIllegalItems => BadArgumentsErrorRunner(BadArgumentsParameter(verb
            , setWithIllegalItems.map(key => (key, key)).toMap
          , allParameter))
        }


        /*
      case OaiCheckedVerbWithParameter(verb, None, None, None, None, None, _, allParameter, configuration) =>
        checkParameterByName(allParameter, AllowedParameter.listSetsAllowed) match {
          case setWithIllegalItems => BadArgumentsErrorRunner(BadArgumentsParameter(verb.get
            , setWithIllegalItems.map(key => (key, key)).toMap
          , allParameter))
        }

      */
        //only as fallack - otherwise code doesn't compile
      case OaiCheckedVerbWithParameter(verb, _, _, _, _, _, _, allParameter, configuration) =>

        //todo: das nochmals anschauen
        checkParameterByName(allParameter, AllowedParameter.listSetsAllowed) match {
          case setWithIllegalItems => BadArgumentsErrorRunner(BadArgumentsParameter(verb.get
            , setWithIllegalItems.map(key => (key, key)).toMap
          , allParameter))

        }


    }

}