/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.common.validation

import org.swissbib.memobase.oai.common.util.ResumptionToken

import org.swissbib.memobase.oai.common.verb.OaiVerb.{GetRecord, Identify, ListIdentifiers, ListMetadataFormats, ListRecords, ListSets, OaiVerb}

import scala.util.Try

sealed abstract class OaiParameterBase() extends Product with Serializable {
  val allQueryParameter: Map[String, Seq[String]]
}

case class ParameterToCheck(verb:Option[OaiVerb],
                            metadataPrefix: Option[String],
                            set: Option[String],
                            from:Option[String],
                            until:Option[String],
                            identifier: Option[String],
                            resumptionToken: Option[String],
                            allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
}



case class CheckedIdentifyParameter (verb:OaiVerb, allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
  require(verb == Identify)

}

case class CheckedGetRecordParameter (verb:OaiVerb, identifier: String, metadataPrefix: String
                                      ,allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
  require(verb == GetRecord)

}

case class CheckedListIdentifiersParameter (verb:OaiVerb, metadataPrefix: String, set:Option[String]
                                            ,from:Option[String], until:Option[String]
                                            ,allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
  require(verb == ListIdentifiers)
}

case class CheckedListIdentifiersParameterExclusive (verb:OaiVerb
                                                     ,resumptionToken: ResumptionToken
                                                    ,allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
  require(verb == ListIdentifiers)
}

case class CheckedListMetadaFormatsParameter(verb:OaiVerb
                                             ,identifier: Option[String]
                                              ,allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
  require(verb == ListMetadataFormats)
}


final case class CheckedListRecordsParameter (verb:OaiVerb,
                                              metadataPrefix: String,
                                              from: Option[String],
                                              until: Option[String]
                                             ,set: Option[String]
                                             ,allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
  require(verb == ListRecords)
}

final case class CheckedListRecordsParameterExclusive (verb:OaiVerb
                                                       ,resumptionToken: ResumptionToken
                                                      ,allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
  require(verb == ListRecords)
}

case class CheckedListSetsParameterExclusive (verb:OaiVerb
                                              ,resumptionToken: String
                                             ,allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
  require(verb == ListSets)
}

case class CheckedListSetsParameter (verb:OaiVerb,
                                     allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase {
  require(verb == ListSets)
}

case class BadArgumentsParameter (verb: OaiVerb
                                  ,badArguments: Map[String,String]
                                  ,allQueryParameter: Map[String, Seq[String]]) extends OaiParameterBase

case class NoContentForQueryParameter (verb: OaiVerb
                                  ,allQueryParameter: Map[String, Seq[String]]
                                     ,errorCode: String ) extends OaiParameterBase


case class SystemErrorParameter (verb: OaiVerb
                                       ,allQueryParameter: Map[String, Seq[String]]
                                       ,errorCode: String
                                        ,errorMessage: String) extends OaiParameterBase



object OAIErrorCode extends Enumeration {

  type OAIErrorCode = Value

  protected case class OAIErrorCodeVal(code: String) extends super.Val {
    //https://www.scala-lang.org/api/current/scala/Enumeration.html
  }

  import scala.language.implicitConversions

  implicit def getCode(x: Value): String = x.asInstanceOf[OAIErrorCodeVal].code

  val NO_RECORDS_MATCH: OAIErrorCodeVal = OAIErrorCodeVal("noRecordsMatch")
  val ID_DOES_NOT_EXIST: OAIErrorCodeVal = OAIErrorCodeVal("idDoesNotExist")
  val SYSTEM_ERROR: OAIErrorCodeVal = OAIErrorCodeVal("idSystemError")


  def getErrorCode(code: Option[String]): Try[OAIErrorCode] = {
    Try[OAIErrorCode](OAIErrorCode.withName(code.getOrElse(throw
      new Exception(s"no code for name $code"))))
  }
}