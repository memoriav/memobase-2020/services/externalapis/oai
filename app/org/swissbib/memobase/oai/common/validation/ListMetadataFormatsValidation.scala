/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



package org.swissbib.memobase.oai.common.validation

import org.swissbib.memobase.oai.runner.{BadArgumentsErrorRunner, ListMetadataFormatsRunner, OaiRunner}


/*
    Arguments
    https://www.openarchives.org/OAI/openarchivesprotocol.html#ListMetadataFormats
    identifier an optional argument that specifies the unique identifier of the item for which available metadata formats are being requested. If this argument is omitted, then the response includes all metadata formats supported by this repository. Note that the fact that a metadata format is supported by a repository does not mean that it can be disseminated from all items in the repository.

  Error and Exception Conditions
  badArgument - The request includes illegal arguments or is missing required arguments.
  idDoesNotExist - The value of the identifier argument is unknown or illegal in this repository.
  noMetadataFormats - There are no metadata formats available for the specified item.

*/


object ListMetadataFormatsValidation extends ParameterValidationFunction {
  override def apply(v1: OaiCheckedVerbWithParameter): OaiRunner =
    v1 match {
      //possible valid combination
      case OaiCheckedVerbWithParameter(verb, None, None, None, None, identifier, None,allQueryParameter, _) =>

        //todo: check valid metadataPrefix
        // check for additional illegal parameter
        // check until not before from
        // check for valid dates

        checkParameterByName(allQueryParameter, AllowedParameter.listMetadataFormatsAllowed) match {
          case Seq() => ListMetadataFormatsRunner(CheckedListMetadaFormatsParameter(verb.get,identifier
            ,allQueryParameter))
          case setWithIllegalItems =>
            BadArgumentsErrorRunner(BadArgumentsParameter(verb.get, setWithIllegalItems.map(key => (key, key)).toMap
            , allQueryParameter))
        }

      case OaiCheckedVerbWithParameter(verb, _, _, _, _, _, _, allParameter, _) =>
        val setWithIllegalItems = checkParameterByName(allParameter, AllowedParameter.listMetadataFormatsAllowed)
        val setWithMissingItems = checkMissingParameterByName(AllowedParameter.listMetadataFormatsMandatory, allParameter)
        BadArgumentsErrorRunner(BadArgumentsParameter(verb.get, setWithIllegalItems.map(key => (key, key)).toMap
        , allParameter))
      //todo: additional case cluases to catch other illegal combinations
    }


}

