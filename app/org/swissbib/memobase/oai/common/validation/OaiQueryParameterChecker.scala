/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.common.validation

import org.swissbib.memobase.oai.common.util.OaiConfig
import org.swissbib.memobase.oai.common.verb.OaiVerb
import org.swissbib.memobase.oai.common.verb.OaiVerb.OaiVerb
import org.swissbib.memobase.oai.runner.{BadArgumentsErrorRunner, OaiRunner}

import scala.util.{Failure, Success}
import play.api.Configuration

case class OaiCheckedVerbWithParameter(verb:Option[OaiVerb],
                                metadataPrefix: Option[String],
                                set: Option[String],
                                from:Option[String],
                                until:Option[String],
                                identifier: Option[String],
                                resumptionToken: Option[String],
                                allQueryParameter: Map[String, Seq[String]],
                                configuration: OaiConfig)

/*
class OaiQueryParameterChecker(verb: Option[String],
                                    metadataPrefix: Option[String],
                                    set: Option[String],
                                    from:Option[String],
                                    until:Option[String],
                                    identifier: Option[String],
                                    resumptionToken: Option[String],
                                    allQueryParameter: Map[String, Seq[String]],
                                    configuration: Configuration) {
  //noinspection ScalaStyle
  def checkQueryParameter: BaseUserRequest = {

    OaiVerb.getVerb(verb) match {
      case Success(value) =>
        value match {
          case OaiVerb.GetRecord => ???
            //GetRecordValidation(
            //OaiCheckedVerbWithParameter(Some(value),metadataPrefix,set,from, until, identifier, resumptionToken,
            //  allQueryParameter, configuration))
          case OaiVerb.Identify => ???
            //IdentifyValidation(
            //OaiCheckedVerbWithParameter(Some(value),metadataPrefix,set,from, until, identifier, resumptionToken,
            //  allQueryParameter, configuration))
          case OaiVerb.ListIdentifiers => ???
            //ListIdentifiersValidation(
            //OaiCheckedVerbWithParameter(Some(value),metadataPrefix,set,from, until, identifier, resumptionToken,
            //  allQueryParameter, configuration))
          case OaiVerb.ListMetadataFormats => ???
            //ListMetadataFormatsValidation(
            //OaiCheckedVerbWithParameter(Some(value),metadataPrefix,set,from, until, identifier, resumptionToken,
            //  allQueryParameter, configuration))
          case OaiVerb.ListRecords => ???
          case OaiVerb.ListSets => ListSetsValidation(
            OaiCheckedVerbWithParameter(Some(value),metadataPrefix,set,from, until, identifier, resumptionToken,
              allQueryParameter, configuration))

            //todo: wie gehe ich hier mit missing vern in der Fehleranzeige um??
          case OaiVerb.WrongVerb | OaiVerb.MissingVerb =>
            //it is possible that users send these not allowed enum values
            //only used as part of the type to round up the workflow
            BadArgumentsReq(BadArgumentsParameter(OaiVerb.MissingVerb
              , Map("badVerb" -> s"Invalid verb $verb in request")
              , allQueryParameter))
        }

      case Failure(_:MissingParameterException) =>
        //todo: see WrongVerb or MissingVerb
        BadArgumentsReq(BadArgumentsParameter(OaiVerb.MissingVerb
          , Map("badVerb" -> s"missing verb in request")
        , allQueryParameter))
      case Failure(_: NoSuchElementException) =>
        //todo: see WrongVerb or MissingVerb
        BadArgumentsReq(BadArgumentsParameter(OaiVerb.MissingVerb
          , Map("badVerb" -> s"Invalid verb $verb in request")
        , allQueryParameter))
      case Failure(exception) =>
        SystemErrorReq(exception)
    }


  }
}



 */
class OaiQueryParameterChecker(verb: Option[String],
                               metadataPrefix: Option[String],
                               set: Option[String],
                               from:Option[String],
                               until:Option[String],
                               identifier: Option[String],
                               resumptionToken: Option[String],
                               allQueryParameter: Map[String, Seq[String]],
                               configuration: OaiConfig) {
  //noinspection ScalaStyle
  def checkQueryParameter: OaiRunner = {

    OaiVerb.getVerb(verb) match {
          case OaiVerb.GetRecord => GetRecordValidation(
            OaiCheckedVerbWithParameter(Some(OaiVerb.GetRecord),metadataPrefix,set,from, until, identifier, resumptionToken,
              allQueryParameter, configuration))
          case OaiVerb.Identify => IdentifyValidation(
            OaiCheckedVerbWithParameter(Some(OaiVerb.Identify),metadataPrefix,set,from, until, identifier, resumptionToken,
              allQueryParameter, configuration))
          case OaiVerb.ListIdentifiers => ListIdentifiersValidation(
            OaiCheckedVerbWithParameter(Some(OaiVerb.ListIdentifiers),metadataPrefix,set,from, until, identifier, resumptionToken,
              allQueryParameter, configuration))
          case OaiVerb.ListMetadataFormats => ListMetadataFormatsValidation(
            OaiCheckedVerbWithParameter(Some(OaiVerb.ListMetadataFormats),metadataPrefix,set,from, until, identifier, resumptionToken,
              allQueryParameter, configuration))
          case OaiVerb.ListRecords => ListRecordsValidation(
            OaiCheckedVerbWithParameter(Some(OaiVerb.ListRecords),metadataPrefix,set,from, until, identifier, resumptionToken,
              allQueryParameter, configuration))
          case OaiVerb.ListSets => ListSetsValidation(
            OaiCheckedVerbWithParameter(Some(OaiVerb.ListSets),metadataPrefix,set,from, until, identifier, resumptionToken,
              allQueryParameter, configuration))
          //todo: wie gehe ich hier mit missing vern in der Fehleranzeige um??
          case OaiVerb.WrongVerb =>
            BadArgumentsErrorRunner(BadArgumentsParameter(OaiVerb.WrongVerb
              , Map("badVerb" -> s"no verb in request")
              , allQueryParameter))

          case OaiVerb.MissingVerb =>
            BadArgumentsErrorRunner(BadArgumentsParameter(OaiVerb.MissingVerb
              , Map("missingVerb" -> s"Invalid verb $verb in request")
              , allQueryParameter))

        }



  }
}


class MissingParameterException(message: String) extends Exception(message)
