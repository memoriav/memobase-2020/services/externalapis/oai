/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



package org.swissbib.memobase.oai.common.validation

import org.swissbib.memobase.oai.common.validation.ListRecordsValidation.checkValidMetadaPrefix
import org.swissbib.memobase.oai.runner.{BadArgumentsErrorRunner, GetRecordRunner, OaiRunner}



/*
https://www.openarchives.org/OAI/openarchivesprotocol.html#GetRecord
**identifier** a required argument that specifies the unique identifier of the item in the repository from which the record must be disseminated.
**metadataPrefix**   a required argument that specifies the metadataPrefix of the format that should be included in the metadata part of the returned record . A record should only be returned if the format specified by the metadataPrefix can be disseminated from the item identified by the value of the identifier argument. The metadata formats supported by a repository and for a particular record can be retrieved using the ListMetadataFormats request.

Error and Exception Conditions
badArgument - The request includes illegal arguments or is missing required arguments.
cannotDisseminateFormat - The value of the metadataPrefix argument is not supported by the item identified by the value of the identifier argument.
idDoesNotExist - The value of the identifier argument is unknown or illegal in this repository.

 */


object GetRecordValidation extends ParameterValidationFunction {
  override def apply(v1: OaiCheckedVerbWithParameter): OaiRunner =
    v1 match {
      //possible valid combination

      case OaiCheckedVerbWithParameter(Some(verb), Some(metadataPrefix), None, None, None, Some(ident), None, allParameter, configuration) =>
        if (!checkValidMetadaPrefix(metadataPrefix,configuration)) {
          BadArgumentsErrorRunner(BadArgumentsParameter(verb,
            Map("metadataPrefix" -> s"$metadataPrefix no valid specification"),
            allParameter))
        } else {

          checkParameterByName(allParameter, AllowedParameter.getRecord) match {
            case Seq() => GetRecordRunner(CheckedGetRecordParameter(verb, ident, metadataPrefix, allParameter))
            case setWithIllegalItems =>
              BadArgumentsErrorRunner(BadArgumentsParameter(verb
                , setWithIllegalItems.map(key => (key, key)).toMap
                , allParameter))
          }
        }

      //todo: make it right - only for completion here
      case OaiCheckedVerbWithParameter(verb, _, _, _, _, _, _, allParameter, _) =>
        val allItems = checkParameterByName(allParameter, AllowedParameter.getRecord).map(key =>
          (key, "argument is illegal")).toMap ++
        checkMissingParameterByName(AllowedParameter.getRecord, allParameter).map(key =>
          (key, "argument is missing")).toMap
        BadArgumentsErrorRunner(BadArgumentsParameter(verb.get, allItems, allParameter ))
      //todo: additional case cluases to catch other illegal combinations
    }

}