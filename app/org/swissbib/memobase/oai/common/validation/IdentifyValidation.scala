/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



package org.swissbib.memobase.oai.common.validation

import org.swissbib.memobase.oai.runner.{BadArgumentsErrorRunner, IdentifyRunner, OaiRunner}

object IdentifyValidation extends ParameterValidationFunction {
  override def apply(v1: OaiCheckedVerbWithParameter): OaiRunner = {
    /*
  Error and Exception Conditions
  badArgument - The request includes illegal arguments.

  Response Format
  The response must include one instance of the following elements:

  repositoryName : a human readable name for the repository;
  baseURL : the base URL of the repository;
  protocolVersion : the version of the OAI-PMH supported by the repository;
  earliestDatestamp : a UTCdatetime that is the guaranteed lower limit of all datestamps recording changes, modifications, or deletions in the repository. A repository must not use datestamps lower than the one specified by the content of the earliestDatestamp element. earliestDatestamp must be expressed at the finest granularity supported by the repository.
  deletedRecord : the manner in which the repository supports the notion of deleted records. Legitimate values are no ; transient ; persistent with meanings defined in the section on deletion.
  granularity: the finest harvesting granularity supported by the repository. The legitimate values are YYYY-MM-DD and YYYY-MM-DDThh:mm:ssZ with meanings as defined in ISO8601.
  */

    //no additional arguments allowed
    v1 match {
      case OaiCheckedVerbWithParameter(verb, None, None, None, None, None, None, allParameter, configuration) =>
        val diffSetParams = checkParameterByName(allParameter, AllowedParameter.identify)
        if (diffSetParams.nonEmpty) {
          BadArgumentsErrorRunner(BadArgumentsParameter(verb.get, diffSetParams.map(key => (key, key)).toMap, allParameter ))
        } else {
          IdentifyRunner(CheckedIdentifyParameter(verb.get, allParameter))
        }

      case OaiCheckedVerbWithParameter(verb, _, _, _, _, _, _, allParameter, configuration) =>
        val diffSetParams = checkParameterByName(allParameter, AllowedParameter.identify)
        if (diffSetParams.nonEmpty) {
          BadArgumentsErrorRunner(BadArgumentsParameter(verb.get, diffSetParams.map(key => (key, key)).toMap, allParameter ))
        } else {
          IdentifyRunner(CheckedIdentifyParameter(verb.get, allParameter))
        }
    }
  }
}