/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.common.validation

import org.swissbib.memobase.oai.common.util.ESResumptionTokenHelper
import org.swissbib.memobase.oai.common.validation.ListRecordsValidation.{checkParameterByName, checkValidMetadaPrefix, checkValidSet}
import org.swissbib.memobase.oai.runner.{BadArgumentsErrorRunner, ListIdentifiersExclusiveRunner, ListIdentifiersRunner, ListRecordsExclusiveRunner, ListRecordsRunner, OaiRunner}

import scala.util.{Failure, Success}


/*
https://www.openarchives.org/OAI/openarchivesprotocol.html#ListIdentifiers
Arguments
**from** an optional argument with a UTCdatetime value, which specifies a lower bound for datestamp-based selective harvesting.
**until** an optional argument with a UTCdatetime value, which specifies a upper bound for datestamp-based selective harvesting.
**metadataPrefix** a required argument, which specifies that headers should be returned only if the metadata format matching the supplied metadataPrefix is available or, depending on the repository's support for deletions, has been deleted. The metadata formats supported by a repository and for a particular item can be retrieved using the ListMetadataFormats request.
**set** an optional argument with a setSpec value , which specifies set criteria for selective harvesting.
**resumptionToken** an exclusive argument with a value that is the flow control token returned by a previous ListIdentifiers request that issued an incomplete list.

Error and Exception Conditions

badArgument - The request includes illegal arguments or is missing required arguments.
badResumptionToken - The value of the resumptionToken argument is invalid or expired.
cannotDisseminateFormat - The value of the metadataPrefix argument is not supported by the repository.
noRecordsMatch- The combination of the values of the from, until, and set arguments results in an empty list.
noSetHierarchy - The repository does not support sets.
 */


//noinspection ScalaStyle
object ListIdentifiersValidation extends ParameterValidationFunction {
  override def apply(v1: OaiCheckedVerbWithParameter): OaiRunner =
    v1 match {

      //possible valid combination
      case OaiCheckedVerbWithParameter(Some(verb), Some(metadataPrefix), Some(set), from, until, None, None, allParameter, configuration) =>

        // check until not before from - really ???
        // check for valid dates


        checkParameterByName(allParameter, AllowedParameter.listIdentifiersAllAllowed) match {
          case Seq() =>
            if (!checkValidSet(set,configuration)) {
              BadArgumentsErrorRunner(BadArgumentsParameter(verb,
                Map("setSpec" -> s"$set no valid specification"),
                allParameter))
            }
            else if (!checkValidMetadaPrefix(metadataPrefix,configuration)) {
              BadArgumentsErrorRunner(BadArgumentsParameter(verb,
                Map("metadataPrefix" -> s"$metadataPrefix no valid specification"),
                allParameter))
            } else {
              ListIdentifiersRunner(CheckedListIdentifiersParameter(verb, metadataPrefix, Some(set), from, until, allParameter))

            }
          case setWithIllegalItems =>
            BadArgumentsErrorRunner(BadArgumentsParameter(verb, setWithIllegalItems.map(key => (key, "")).toMap
              , allParameter))
        }




      //only resumption token with nothing else is possible

      //todo: hier falle ich hinein wenn ich "kein" metadata prefix habe aber auch kein resumption token, dann gibt es
      // einen Fehler!
      // dringend beheben!

      case OaiCheckedVerbWithParameter(Some(verb), Some(metadataPrefix),None, from, until, None, None, allParameter
      //request with no set
      , configuration) =>

        checkParameterByName(allParameter, AllowedParameter.listIdentifiersAllAllowed) match {
          case Seq() =>
            if (!checkValidMetadaPrefix(metadataPrefix,configuration)) {
              BadArgumentsErrorRunner(BadArgumentsParameter(verb,
                Map("metadataPrefix" -> s"$metadataPrefix no valid specification"),
                allParameter))
            } else {

              ListIdentifiersRunner(CheckedListIdentifiersParameter(verb, metadataPrefix, None, from, until, allParameter))
            }
          case setWithIllegalItems =>
            BadArgumentsErrorRunner(BadArgumentsParameter(verb, setWithIllegalItems.map(key => (key, "")).toMap
              , allParameter))
        }



      case OaiCheckedVerbWithParameter(verb, None, None, None, None, None, Some(resumptionToken), allParameter, configuration) =>


        ESResumptionTokenHelper.createResumptionTokenFromOaiToken(resumptionToken) match {
          case Success(rt) =>
            checkParameterByName(allParameter, AllowedParameter.listIdentifiersResumption) match {
              case Seq() =>
                ListIdentifiersExclusiveRunner(CheckedListIdentifiersParameterExclusive(
                  verb.get
                  ,rt
                  ,allParameter))
              case setWithIllegalItems => BadArgumentsErrorRunner(BadArgumentsParameter(verb.get
                , setWithIllegalItems.map(key => (key, key)).toMap
                , allParameter))
            }
          case Failure(exceptionRT) =>
            BadArgumentsErrorRunner(BadArgumentsParameter(verb.get,
              Map("resumptionToken" -> exceptionRT.getMessage),
              allParameter))

        }



      case OaiCheckedVerbWithParameter(verb, _, _, _, _, _, _, allParameter, configuration) =>

        //todo: das nochmals anschauen
        BadArgumentsErrorRunner(BadArgumentsParameter(verb.get,
          Map("illegalParameter" -> "no valid parameter"),
          allParameter))






    }


}