/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.runner

import modules.{OAIContent, OaiRepository}
import org.swissbib.memobase.oai.common.validation.{CheckedGetRecordParameter, NoContentForQueryParameter, OAIErrorCode, SystemErrorParameter}
import org.swissbib.memobase.oai.response.{GetRecordResponse, OaiResponse}
import play.api.Configuration

import scala.util.{Failure, Success}

case class GetRecordRunner(parameter: CheckedGetRecordParameter) extends OaiRunner {
  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse = {

    //noinspection ScalaStyle
    repository.getRecord(parameter.identifier,parameter.metadataPrefix) match {
      case Success(result: OAIContent) =>
        GetRecordResponse(config, repository, result,parameter)
      case Failure(exception) =>
        exception match {
          case e: GetRecordFailure =>

            NoContentForQueryRunner(NoContentForQueryParameter(parameter.verb
            , parameter.allQueryParameter
            , OAIErrorCode.ID_DOES_NOT_EXIST)).run()
            //todo: make something useful for system errors
          case ee  =>
            SystemErrorRunner(SystemErrorParameter(parameter.verb,parameter.allQueryParameter,
              OAIErrorCode.SYSTEM_ERROR,exception.getMessage)).run()

        }
    }
  }
}
