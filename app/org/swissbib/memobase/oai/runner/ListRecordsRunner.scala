/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */




package org.swissbib.memobase.oai.runner

import modules.OaiRepository
import org.swissbib.memobase.oai.common.validation.{CheckedListRecordsParameter, CheckedListRecordsParameterExclusive, NoContentForQueryParameter, OAIErrorCode, SystemErrorParameter}
import org.swissbib.memobase.oai.response.{ListRecordsResponse, OaiResponse}
import play.api.Configuration

import scala.util.{Failure, Success}

case class ListRecordsRunner(parameter: CheckedListRecordsParameter) extends OaiRunner {

  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse = {
    //ich benötige einen genrischen Typ result

    repository.listRecords(from = parameter.from,
      parameter.until,
      parameter.set,
      Option.empty,
      parameter.metadataPrefix) match {
      case Success(requestResult: ResultList) =>
        //todo: make it mor functional
        requestResult.result match {
            //no result
          case Nil => NoContentForQueryRunner(NoContentForQueryParameter(parameter.verb
            , parameter.allQueryParameter
            ,OAIErrorCode.NO_RECORDS_MATCH)).run()
          //at least one result
          case _ =>  ListRecordsResponse(config,repository,requestResult, parameter)
        }
      case Failure(exception) =>
        SystemErrorRunner(SystemErrorParameter(parameter.verb,parameter.allQueryParameter,
          OAIErrorCode.SYSTEM_ERROR,exception.getMessage)).run()
    }
  }
}

case class ListRecordsExclusiveRunner(parameter: CheckedListRecordsParameterExclusive) extends OaiRunner {
  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse = {

    repository.listRecords(
      from = Option.empty,
      until = Option.empty,
      set = Option.empty,
      resumptionToken = Option(parameter.resumptionToken),
      metadataPrefix = "4711") match {
      case Success(requestResult) =>
        requestResult.result match {
          //no result - although this should not happen in reality
          case Nil => NoContentForQueryRunner(NoContentForQueryParameter(parameter.verb
            , parameter.allQueryParameter
            ,OAIErrorCode.NO_RECORDS_MATCH)).run()
          //at least one result
          case _ =>  ListRecordsResponse(config,repository,requestResult, parameter)
        }

      case Failure(exception) => ??? //ListRecordsResponse(this, null)
    }
  }
}
