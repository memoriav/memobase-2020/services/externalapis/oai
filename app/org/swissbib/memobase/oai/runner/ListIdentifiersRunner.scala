/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.runner

import modules.OaiRepository
import org.swissbib.memobase.oai.common.validation.{CheckedListIdentifiersParameter, CheckedListIdentifiersParameterExclusive, NoContentForQueryParameter, OAIErrorCode, SystemErrorParameter}
import org.swissbib.memobase.oai.response.{ListIdentifiersResponse, ListRecordsResponse, OaiResponse}
import play.api.Configuration

import scala.util.{Failure, Success}

case class ListIdentifiersRunner(parameter: CheckedListIdentifiersParameter) extends OaiRunner {

  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse = {

    repository.listIdentiers(
      from = parameter.from,
      until = parameter.until,
      set = parameter.set,
      resumptionToken = Option.empty,
      metadataPrefix = parameter.metadataPrefix) match {
      case Success(requestResult: ResultList) =>
        requestResult.result match {
            //empty result
          case Nil => NoContentForQueryRunner(NoContentForQueryParameter(parameter.verb
            , parameter.allQueryParameter
            ,OAIErrorCode.NO_RECORDS_MATCH)).run()
          case _ =>  ListIdentifiersResponse(config,repository,requestResult, parameter)
        }
      case Failure(exception) =>
        SystemErrorRunner(SystemErrorParameter(parameter.verb,parameter.allQueryParameter,
          OAIErrorCode.SYSTEM_ERROR,exception.getMessage)).run()
    }




  }
}

case class ListIdentifiersExclusiveRunner(parameter: CheckedListIdentifiersParameterExclusive)  extends OaiRunner {
  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse =

    repository.listIdentiers(
      from = Option.empty,
      until = Option.empty,
      set = Option.empty,
      resumptionToken = Option(parameter.resumptionToken),
      metadataPrefix = "4711") match {
      case Success(requestResult) =>
        requestResult.result match {
          //empty result although thi should not happen - one never knows...
          case Nil => NoContentForQueryRunner(NoContentForQueryParameter(parameter.verb
            , parameter.allQueryParameter
            ,OAIErrorCode.NO_RECORDS_MATCH)).run()
          //at least one result
          case _ =>  ListIdentifiersResponse(config,repository,requestResult, parameter)
        }
      case Failure(exception) => ??? //ListRecordsResponse(this, null)
    }

}
