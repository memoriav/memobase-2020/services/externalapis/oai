/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.runner

import modules.OaiRepository
import org.swissbib.memobase.oai.common.validation.{CheckedListSetsParameter, CheckedListSetsParameterExclusive}
import org.swissbib.memobase.oai.response.{ListSetsResponse, OaiResponse}
import play.api.Configuration

case class ListSetsRunner(parameter: CheckedListSetsParameter) extends OaiRunner {
  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse = {

    ListSetsResponse(config,repository,parameter)
  }
}

case class ListSetsExclusiveRunner(parameter: CheckedListSetsParameterExclusive) extends OaiRunner {

  //todo: brauchen wir im Moment nicht. s. dazu auch Kommentar in ListSetsValidation
  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse = ???
}
