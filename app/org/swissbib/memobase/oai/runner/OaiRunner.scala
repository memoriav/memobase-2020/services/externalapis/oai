/*
 * generic OAI Server - agnostic in relation to the data repository
 * initially created for memobase project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package org.swissbib.memobase.oai.runner

import modules.OaiRepository
import org.swissbib.memobase.oai.common.validation.{BadArgumentsParameter, NoContentForQueryParameter, SystemErrorParameter}
import org.swissbib.memobase.oai.response.{BadArgumentsResponse, NoContentForQueryResponse, OaiResponse, SystemErrorResponse}
import play.api.Configuration


abstract class OaiRunner {
  def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse
}



case class NoContentForQueryRunner(parameter: NoContentForQueryParameter) extends OaiRunner {

  //todo: Es fehlt noch eine Response für no content
  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse =
    NoContentForQueryResponse(config,repository,parameter)

}

//todo: make more detailed types
//todo: brauchen wir das?
//case class GetExceptionRunner(config: Configuration, repository: OaiRepository, request:
//                                                          BadArgumentsReq) extends OaiRequestRunner {
//  override def run(): OaiResponse = ???
//}


case class BadArgumentsErrorRunner(parameter: BadArgumentsParameter) extends OaiRunner {

  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse =
    BadArgumentsResponse(config, repository,parameter)
}

case class SystemErrorRunner(parameter: SystemErrorParameter) extends OaiRunner {

  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse =
    SystemErrorResponse(config, repository,parameter)
}




