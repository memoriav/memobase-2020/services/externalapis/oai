/*
 * OAI Server Interface
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sbt.*

object Dependencies {

  lazy val joda = "org.joda" % "joda-convert" % "2.2.+"
  lazy val logging = "net.logstash.logback" % "logstash-logback-encoder" % "7.4.+"
  lazy val lemonlabs = "io.lemonlabs" %% "scala-uri" % "4.0.+"
  lazy val codingwell = "net.codingwell" %% "scala-guice" % "7.0.+"
  // "org.apache.solr" % "solr-solrj" % "7.3.1",
  // "com.fasterxml.jackson.core" % "jackson-databind" % "2.10.1",
  // "org.marc4j" % "marc4j" % "2.9.1",
  // "org.z3950.zing" % "cql-java" % "1.12",
  lazy val scalatestplusplay =
    "org.scalatestplus.play" %% "scalatestplus-play" % "7.0.+"

  lazy val elasticsearch = "org.elasticsearch.client" % "elasticsearch-rest-high-level-client" % "7.7.0"

  lazy val json2xml = "de.odysseus.staxon" % "staxon" % "1.3.+"
  lazy val upickle = "com.lihaoyi" %% "upickle" % "3.3.+"

  lazy val scala_xml_module = "org.scala-lang.modules" %% "scala-xml" % "2.3.0"
  lazy val httprequests = "com.lihaoyi" %% "requests" % "0.8.+"

  lazy val jwtcore = "com.github.jwt-scala" %% "jwt-core" % "10.+"

  lazy val saxonhe = "net.sf.saxon" % "Saxon-HE" % "12.5.+"

}
